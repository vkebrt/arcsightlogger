package main

/*
 * import libs
 */

import (
	"log"
	"time"
	"loggerapi/pkg/api"
)


func main() {
	log.Printf("[Info] loggerquery starting up...")
	defer log.Printf("[Info] loggerquery ending...")

	// setup login - for now my own as mock vals
	auth := api.NewAUTH()
	auth.SetUsername("vkebrt")
	auth.SetPassword("Provoz#Banda#Kretenu!")
	api.BaseURL = "https://10.31.123.159:9000/"
	
	// call login
	token, err := auth.Login()
	if err != nil {
		log.Fatalf("[Error] on logging into logger: %v", err)
	}

	// for now
	log.Printf("[Debug] gotted token: %s", token)
	

	etime := time.Now()
	stime := etime.Add(-5*time.Minute)

	_, err = auth.StartSEARCH(
				stime, 
				etime, 
				"[\"Event Time\", \"Device\", \"Logger\", \"Raw Message\", \"deviceVendor\", \"deviceProduct\", \"deviceVersion\", \"deviceEventClassId\", \"name\"]",
				[]byte("[destinationAddress=\"77.75.75.172\"]"),
				3600000 )



	// closeup
	err = auth.Logout()
	if err != nil {
		log.Fatalln(err)
	}
}
