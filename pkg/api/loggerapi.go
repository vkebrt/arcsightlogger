package api

/*
 * Import necesary libs 
 */
import (
	"bytes"
	"crypto/tls"
	"encoding/json"
//	"fmt"
//	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"
)

/* 
 * Constants 
 */
const (
	Api_Auth_Login = "/core-service/rest/LoginService/login"
	Api_Auth_Logout = "/core-service/rest/LoginService/logout"
	Api_Search = "/server/search"
	Api_Search_status = "/server/search/status"
	Api_Search_events = "/server/search/events"
	Api_Search_close = "/server/search/close"
	Api_Search_raw_events = "/server/search/raw_events"
)

/*
 * Global variables, our types and so 
 */

// Base url aka https://host:port/
var BaseURL string

// Structure contains data for authentication
type Auth struct {
	Username string
	Password string
	Token string
	LoginDT time.Time
	LogoutDT time.Time
}

// Structure for decompose authok response
type AuthOK struct {
	LogLoginResponse struct {
		LogReturn string `json:"log.return"`
	} `json:"log.loginResponse"`
}

// for creation of search in logger
type Search struct {
  SessionID       int     `json:"search_session_id"`
  UserSessionID   string  `json:"user_session_id"`
  DiscoverFields  bool    `json:"discover_fields"`
  EndTime         string  `json:"end_time"`
  StartTime       string  `json:"start_time"`
  FieldSummary    bool    `json:"field_summary"`
  SummaryFields   string  `json:"summary_fields"`
  LocalSearch     bool    `json:"local_search"`
  Query           []byte  `json:"query"`
  SearchType      string  `json:"search_type"`
  Timeout         int     `json:"timeout"`
}

/* 
 * Functions for our types 
 */

func NewAUTH() (*Auth) {
	return &Auth{}
}

func (a *Auth) SetUsername(username string) {
	a.Username = username
}

func (a *Auth) SetPassword(passwd string) {
	a.Password = passwd
}

func (a *Auth) GetUsername() (string) {
	return a.Username

}

func (a *Auth) GetPassword() (string) {
	return a.Password
}

func DoRequest(url string, data []byte, content_type string) ([]byte, error) {
	// client instance
	clh := &http.Client{}
		
	// disable crt check (most loggers have non public crts)
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	// debug
	log.Printf("[Debug] post body: %s", data)

	// data transform
	pdata := strings.NewReader(string(data))

	// prepare request	
	req, err := http.NewRequest("POST", url, pdata)
	if err != nil {
		log.Printf("[Error] on preparing request: %v", err)
		return nil, err
	}
	req.Header.Add("Accept","application/json")
	req.Header.Add("Content-Type", content_type)
	
	// debug
	log.Printf("POST struct: %+v", req)


	// execute query
	resp, err := clh.Do(req)
	if err != nil {
		log.Printf("[Error] on execution of post: %+v, %v", req, err)
		return nil, err
	}
	defer resp.Body.Close()
	
	// read response data and return them
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("[Error] on reading response data: %v", err)
		return nil, err
	}

	// return data
	return body, nil
}


func (a *Auth) Login() (string, error) {
	log.Printf("[Info] doing login into logger API...")
	// prepare values
	values := url.Values{}
	values.Set("login", a.Username)
	values.Set("password", a.Password)
	
	// prepare login url
	url := BaseURL+Api_Auth_Login

	// content type encoding
	content_type := "application/x-www-form-urlencoded"

	// prepare data to post
	ldata := []byte(values.Encode())

	// read response
	body, err := DoRequest(url, ldata, content_type)
	if err != nil {
		log.Printf("[Error] on reading login result data: %s", err.Error())
		return "", err
	}

	// now decode returned json
	var OkLogin AuthOK
	err = json.Unmarshal(body, &OkLogin)
	if err != nil {
		log.Printf("[Error] on decoding login response: %s", err.Error())
		return "", err
	}
	token := OkLogin.LogLoginResponse.LogReturn
	log.Printf("[Debug] got token %s from login process...", token)
	a.Token = token
	
	log.Printf("[Info] Login ok...")
	return token, nil
}

func (a *Auth) Logout() (error){
	log.Printf("[Info] doing logout from logger API...")
	//prepare values
	values := url.Values{}
	values.Set("authToken",a.Token)
	
	// prepare logout url
	url := BaseURL+Api_Auth_Logout
	
	// content type encoding
	cte := "application/x-www-form-urlencoded"
	
	// prep data for post
	pdata := []byte(values.Encode())

	// read response
	body, err := DoRequest(url, pdata, cte)
	if err != nil {
		log.Printf("[Error] on reading logout result data: %s", err.Error())
		return err
	}
	// debug print logout response
	log.Printf("[Info] logout response: %s", body)

	log.Printf("[Info] Logout success...")
	return nil
}

func NewSEARCH() (*Search) {
	return &Search{}
}

func (a *Auth) StartSEARCH(startTIME time.Time, endTIME time.Time, fields string, query []byte, timeout int) (string,error) {
	log.Printf("[Info] starting search on query: %s", query)
	// prepare search configuration	
	src := NewSEARCH()
	src.SessionID = rand.Intn(65535)
	src.UserSessionID = string(a.Token)
	src.DiscoverFields = false
	src.EndTime = endTIME.Format("RFC3339Nano")
	src.StartTime = startTIME.Format("RFC3339Nano")
	src.FieldSummary = false
	src.LocalSearch = false
	src.SearchType = "interactive"
	src.Timeout = timeout	
	src.SummaryFields = fields
	src.Query = query
	
	log.Printf("[Debug] search struct filled in: %+v", src)
	
	// prepare query data
	buf := new(bytes.Buffer)
	if err := json.NewEncoder(buf).Encode(src); err != nil {
		log.Printf("[Error] on encoding data for start search : %v", err)
		return "", err
	}

	// prepare url
	url := BaseURL + Api_Search
	
	// content type
	ct := "application/json"

	// do request
	respBody, err := DoRequest(url, buf.Bytes(), ct)
	if err != nil {
		log.Printf("[Error] on starting search: %v", err)
		return "", err
	}

	//  read response, for now just print it out
	log.Printf("[Debug] response from start search: %s", respBody)

	return string(respBody), nil
}
	
		
		
